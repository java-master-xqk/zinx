package main

/**
第一个Iris程序
*/

import "github.com/kataras/iris/v12"

func main() {
	// 1. 创建app结构体对象
	//app := iris.New()
	app := iris.New()

	// 2. 端口监听
	//app.Run(iris.Addr(":8000"), iris.WithoutServerError(iris.ErrServerClosed))
	app.Run(iris.Addr(":8000"))

}
