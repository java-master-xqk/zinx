package main

import (
	"encoding/json"
	"fmt"
	"os"
)

// ConfigInfo 这个结构体根据json文件内的信息变化, 需要读什么, 内部就取什么属性,
// ConfigInfo 注意属性名要大写, 否则无法访问struct内部属性
type ConfigInfo struct {
	ProjectName string
	Port        int
}

func main() {
	jsonPath := "D:\\GoPeoject\\Iris-admin\\5-RouterGroup\\configs\\config.json"
	var conf ConfigInfo
	// 解析JSON配置文件
	ParseJsonFile(&conf, jsonPath)

	fmt.Println(conf)
	fmt.Println(conf.Port)
	fmt.Println(conf.ProjectName)

}

// ParseJsonFile 用来解析json文件的, 需要传入json文件对应的struct类型指针用来接收信息
// ParseJsonFile 还需要一个json文件的地址 jsonPath
func ParseJsonFile(conf *ConfigInfo, jsonPath string) {
	fileObject, _ := os.Open(jsonPath)
	defer fileObject.Close()
	decoder := json.NewDecoder(fileObject)
	err := decoder.Decode(&conf)
	if err != nil {
		fmt.Println("解析JSON文件失败")
	} else {
		fmt.Println("解析JSON文件成功")
	}
}
