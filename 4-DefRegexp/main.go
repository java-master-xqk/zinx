package main

import (
	"fmt"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/context"
	"reflect"
)

/**
自定义正则表达式`
*/

func main() {
	app := iris.New()

	/**
	1. handle方式处理
	*/
	// GET : http://localhost:8000/userinfo
	app.Handle("GET", "/userinfo", func(context context.Context) {
		path := context.Path()
		app.Logger().Info("请求的路径为 : ", path)
		app.Logger().Error("request path : ", path)
	})

	// post
	app.Handle("POST", "/postCommit", func(c context.Context) {
		path := c.Path()
		app.Logger().Info("请求路径为 : ", path)
		c.HTML(path)
	})

	// http:localhost:8000?data=20220523&city=ShenZhen
	// http:localhost:8000/weather/2022-05-23/ShenZhen
	// http:localhost:8000/weather/2022-05-23/BeiJing
	// http:localhost:8000/weather/1949-10-01/Beijing
	//正则表达式 : {}
	app.Get("/weather/{data}/{city}", func(c context.Context) {
		path := c.Path()
		app.Logger().Info("请求的路径为: ", path)

		data := c.Params().Get("data")
		city := c.Params().Get("city")

		c.JSON(iris.Map{
			"status":  200,
			"data":    data,
			"city":    city,
			"message": "成功拿到URL的参数",
		})

	})

	/**
	1. Get 正则表达式 : {name}
	使用 : context.Params().get("name")
	*/
	app.Get("/hello/{name}", func(c context.Context) {
		path := c.Path()
		app.Logger().Info("请求的路径为 : ", path)

		name := c.Params().Get("name")

		c.HTML("<h1>" + name + "<h1>")
	})

	/**
	2.自定义正则表达式变量路由请求 {unit64, unit64}进行变量限制
	即 如果没输入限制的类型, 那URL会找不到page, 显示404
	以这种方式限制括号里的变量类型
	*/
	app.Get("/api/user/{userid:uint64}", func(c context.Context) {
		userId, err := c.Params().GetUint("userid")
		if err != nil {
			app.Logger().Error("类型检测错误")
		}
		typeOfId := reflect.TypeOf(userId)

		fmt.Println(typeOfId)
		c.JSON(iris.Map{
			"status":   200,
			"data":     userId,
			"dataType": typeOfId,
			"message":  "ok",
		})
	})

	// 自定义正则表达式路由请求 bool
	app.Get("/api/user/{isLogin:bool}", func(c context.Context) {
		isLogin, err := c.Params().GetBool("isLogin")
		if err != nil {
			c.StatusCode(iris.StatusBadRequest)
			return
		}
		if isLogin {
			c.WriteString("登陆成功")
		} else {
			c.WriteString("登陆失败")
		}

	})

	app.Run(iris.Addr(":8000"))
}
