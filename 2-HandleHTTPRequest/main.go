package main

/**
处理HTTP请求
*/

import (
	"fmt"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/context"
)

type Person struct {
	Username string `json:"username" xml:"username"`
	Password string `json:"password" xml:"password"`
}

func main() {
	app := iris.New()

	app.Get("/getRequest", func(context context.Context) {
		// 处理get请求, 清秋阁url为 /getRequest
		path := context.Path()
		app.Logger().Info(path)
	})

	// 处理Get请求
	app.Get("/userpath", func(c context.Context) {
		path := c.Path()
		app.Logger().Info(path)
		// WriteString 把服务器的想要相应的信息 写到了返回值里
		c.WriteString("请求路径" + path)
	})

	// 2.处理Get请求, 并接收参数
	app.Get("/userinfo", func(c context.Context) {
		path := c.Path()
		app.Logger().Info(path)

		userName := c.URLParam("username")
		app.Logger().Info(userName)

		password := c.URLParam("pwd")
		app.Logger().Info(password)
		c.HTML("<h1>" + userName + "," + password + "</h1>")

		//c.WriteString("请求路径" + path)

	})

	app.Get("/talk", func(c context.Context) {
		path := c.Path()
		app.Logger().Info(path)

		maps := map[string]interface{}{}
		c.ReadJSON(&maps)
		fmt.Println(maps)

	})

	// 3. 处理Post请求, form表单的字段获取
	app.Post("/postLogin", func(c context.Context) {
		path := c.Path()
		app.Logger().Info(path)

		name := c.PostValue("name")
		password := c.PostValueDefault("pwd", "默认密码")
		app.Logger().Info(name + "," + password)

		c.HTML(name)
	})

	// 4. 处理Post请求, Json格式数据
	app.Post("/postJson", func(c context.Context) {
		// 1. path
		path := c.Path()
		app.Logger().Info("请求url" + path)

		// 2. Json数据解析
		//var user User
		var person Person // Person结构体 内的属性一定要大写, 不然结构体外面无法访问

		if err := c.ReadJSON(&person); err != nil {
			c.JSON(iris.Map{
				"status":  400,
				"message": "传入值失败",
			})
		} else {
			c.JSON(iris.Map{
				"status":   200,
				"message":  "传入值成功",
				"username": person.Username,
				"password": person.Password,
			})
		}
	})

	app.Post("/postXML", func(c context.Context) {
		// 1.path
		path := c.Path()
		app.Logger().Info("请求" + path)

		// 2.XML数据解析, 要注意在结构体内的属性后面添加
		//`xml:"username"`
		//`xml:"password"`
		var person Person
		if err := c.ReadXML(&person); err != nil {
			c.JSON(iris.Map{
				"status":  400,
				"message": "解析XML失败",
			})
		} else {
			c.JSON(iris.Map{
				"status":  200,
				"data":    person,
				"message": "解析成功",
			})
		}
	})

	app.Run(iris.Addr(":8000"))
}
