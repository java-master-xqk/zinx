package main

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
)

type UserController struct {
}

func main() {
	app := iris.New()
	//6.1-mvc.New(app).Handle(new(UserController))

	// 路由组的mvc处理
	mvc.Configure(app.Party("/user"), func(context *mvc.Application) {
		//把user这个路由组 映射到 UserController这个struct里去, 只要是UserController启动的方法, 都是这个路由组下面的
		context.Handle(new(UserController))
	})

	app.Run(iris.Addr(":8000"))
}

//Get url : http://localhost:8000
//Get type : get
func (uc *UserController) Get() string {
	iris.New().Logger().Info("get请求")

	return "hello world"
}

// Post url : http://localhost:8000
// Post type : post
func (uc *UserController) Post() {
	iris.New().Logger().Info("post 请求")
}

// Put url : http://localhost:8000
// Put type : put
func (uc *UserController) Put() {
	iris.New().Logger().Info("put 请求")
}

/**
根据请求类型和请求URL自动匹配处理方法
*/

// GetInfo url : http://localhost:8000/info
// GetInfo type : get
func (uc *UserController) GetInfo() mvc.Result {
	iris.New().Logger().Infof("get 请求")

	return mvc.Response{
		Code: iris.StatusBadRequest,
		Object: map[string]interface{}{
			"status":  iris.StatusOK,
			"data":    "MVC response",
			"message": "ok",
		},
	}
}

// ----------------------------第二种寻址URL映射方式---------------------------------------------------------------

// BeforeActivation 自己指定, 另一种方式
// BeforeActivation 这是另一种 URL映射接口的方式
func (uc *UserController) BeforeActivation(a mvc.BeforeActivation) {

	a.Handle("GET", "/query", "UserInfo")
}

// UserInfo url : http://localhost:8000/query
// UserInfo type : get
func (uc *UserController) UserInfo() mvc.Result {
	iris.New().Logger().Info("user info query")

	return mvc.Response{
		Code: iris.StatusOK,
		Object: map[string]interface{}{
			"username": "xqk",
			"password": "asdadsa",
			"role":     1,
		},
	}
}
