package main

import (
	"fmt"
	"github.com/kataras/iris/v12"
	"net/http"
)

/**
当我们在iris种讨论中间件的似乎和, 我们就是在讨论运行一个HTTP请求处理器前后的代码.
例如 : 日志中间件会把即将到来的请求明细写到日志中, 然后在写响应日志之前, 调用处理器代码.
这些中间件是松耦合, 而且可重用.
*/

func main() {
	app := iris.New()

	app.Get("/", before, mainHandler, after)

	app.Run(iris.Addr(":8080"))
}

func before(ctx iris.Context) {
	shareInfomation := "this is sharable information between handlers"

	requestPath := ctx.Path()
	fmt.Println("在mainHandler之前 : " + requestPath)

	ctx.Values().Set("info", shareInfomation)
	ctx.Next() // 执行下一个处理器
}

func after(ctx iris.Context) {
	fmt.Println("在mainHandler之后")
	fmt.Println(ctx.Values().GetStringTrim("info"))
}

func mainHandler(ctx iris.Context) {
	fmt.Println("在mainHandler里面")

	info := ctx.Values().GetStringTrim("info")
	ctx.JSON(iris.Map{
		"status":  http.StatusOK,
		"info":    info,
		"message": "response",
	})
	ctx.Next() // 执行after
}
