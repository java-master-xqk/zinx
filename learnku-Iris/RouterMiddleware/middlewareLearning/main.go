package main

import (
	"fmt"
	"github.com/kataras/iris/v12"
)

func main() {
	app := iris.New()

	// 注册before, 处理器作为当前域名所有路由中第一个处理函数
	// 或者使用 UseGlobal 去注册一个中间件, 用于在所有子域名种使用
	app.Use(before)

	// 注册 after, 在所有路由的处理程序之后调用
	app.Done(after)

	// 注册路由
	app.Get("/", indexHandler)
	app.Get("/contact", contactHandler)

	app.Run(iris.Addr(":8080"))
}

func before(c iris.Context) {
	// todo
	fmt.Println("before开始")
}

func after(c iris.Context) {
	// todo
	fmt.Println("after开始")
}

func indexHandler(c iris.Context) {
	// 响应客户端
	c.HTML("<h1>index</h1>")
	c.Next() // 执行通过Done注册的"after"处理器
}

func contactHandler(c iris.Context) {
	// 响应客户端
	c.HTML("<h1>contact</h1>")
	c.Next() // 执行通过 Done注册的"after"处理器
}
