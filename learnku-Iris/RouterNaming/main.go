package main

import "github.com/kataras/iris/v12"

func main() {
	app := iris.New()
	h := func(ctx iris.Context) {
		route := ctx.GetCurrentRoute()
		app.Logger().Info(route)
		ctx.HTML("<b>Hi<b1>")
	}

	// 处理器注册和命名
	home := app.Get("/", h)
	home.Name = "homeFunc"

	// 或者
	app.Get("/about", h).Name = "about"
	app.Get("/page/{id:int min(0)}", h).Name = "page"

	app.Run(iris.Addr(":8080"))
}
