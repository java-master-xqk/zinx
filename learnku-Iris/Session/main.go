package main

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/sessions"
)

var (
	cookieNameForSessionID = "mycookiesessionnameid"
	sess                   = sessions.New(sessions.Config{Cookie: cookieNameForSessionID})
)

func secret(ctx iris.Context) {
	//检查用户是否已通过身份验证
	auth, _ := sess.Start(ctx).GetBoolean("authenticated")
	if !auth {
		ctx.StatusCode(iris.StatusForbidden)
		return
	}

	//打印秘密消息
	ctx.WriteString("The cake is lie")
}

func login(ctx iris.Context) {
	session := sess.Start(ctx)

	//在此进行身份验证
	// ...

	// 将用户设置为已验证
	session.Set("authenticated", true)
}

func logout(ctx iris.Context) {
	session := sess.Start(ctx)

	//撤销用户身份验证
	session.Set("authenticated", false)
}

func main() {
	app := iris.New()
	app.Get("/secret", secret)
	app.Get("/login", login)
	app.Get("/logout", logout)

	app.Run(iris.Addr(":8080"))
}




