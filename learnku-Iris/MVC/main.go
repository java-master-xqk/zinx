package main

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/middleware/logger"
	"github.com/kataras/iris/v12/middleware/recover"
	"github.com/kataras/iris/v12/mvc"
)

func main() {
	app := iris.New()

	app.Use(recover.New())
	app.Use(logger.New())

	mvc.New(app).Handle(new(ExampleController))

	// http://localhost:8080
	// http://localhost:8080/ping
	// http://localhost:8080/hello
	// http://localhost:8080/custom_path
	app.Run(iris.Addr(":8080"))
}

// ExampleController 服务于 / , /ping, /hello 路由
type ExampleController struct{}

// Get GET http://localhost:8080
func (c *ExampleController) Get() mvc.Result {
	return mvc.Response{
		ContentType: "text/html",
		Text:        "<h1>Welcome</h>",
	}
}

// GetHello GET http://localhost:8080/hello
func (c *ExampleController) GetHello() interface{} {
	return map[string]string{"message": "Hello Iris!"}
}

/**
BeforeActivation会被调用一次, 在控制器适应主应用程序之前
并且当然也是在服务运行之前
*/

//func (c *ExampleController) BeforeActivation(b mvc.BeforeActivation) {
//	f := func(ctx iris.Context) {
//		ctx.Application().Logger().Warnf("Inside / custom_path")
//		ctx.Next()
//	}
//	//todo
//
//}
