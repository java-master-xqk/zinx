package main

import (
	"fmt"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
	"github.com/kataras/iris/v12/sessions"
	"time"
)

type VisitController struct {
	Session *sessions.Session

	StartTime time.Time
}

// Get GET  http://localhost:8080
func (c *VisitController) Get() string {
	visits := c.Session.Increment("visits", 1)
	since := time.Now().Sub(c.StartTime).Seconds()
	return fmt.Sprintf("%d visit from my current session in %0.1f seconds of server's up-time",
		visits, since)
}

func newApp() *iris.Application {
	app := iris.New()
	sess := sessions.New(sessions.Config{Cookie: "mysession_cookie_name"})

	visitApp := mvc.New(app.Party("/"))

	visitApp.Register(sess.Start, time.Now())

	visitApp.Handle(new(VisitController))

	return app
}

func main() {

	app := newApp()

	app.Run(iris.Addr(":8080"))
}
