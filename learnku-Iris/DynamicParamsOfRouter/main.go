package main

/**
动态路由参数
*/
import (
	"github.com/kataras/iris/v12"
	"net/http"
)

func main() {
	app := iris.New()

	// 对于一个可以是任何类型的单个路径参数, 你都可以使用 string类型
	app.Get("/username/{name}", func(context iris.Context) {
		context.Writef("hello %s", context.Params().Get("name"))
	}) // 省略类型 = {name:string}

	// 让我们注册我们第一个添加到int类型的宏函数
	// min = 函数名称
	// minValue = 函数参数
	// func (string) bool = 宏的路径参数的求值函数, 当用户请求包含min()
	// 宏的 : int 路径参数的URL时, 这个函数将会执行
	// todo

	// http://localhost:8080/profile/id>=1
	// 如果路由是这样, 那么 /profile/0 ,  /profile/blabla , /profile/-1 都将抛出404错误
	// 宏函数参数是可以省略的
	app.Get("/profile/{id:int min(1)}", func(ctx iris.Context) {
		// 第二个参数是错误的, 但是它将是nil, 因为我们使用了宏
		// 验证已经执行了
		id, _ := ctx.Params().GetInt8("id")
		ctx.JSON(iris.Map{
			"status":  200,
			"id":      id,
			"message": "ok",
		})
	})

	// 改变路径参数错误引起的错误码
	app.Get("/profile/{id:int min(1) else 503}/friends/{fid:int min(1) else 504}", func(ctx iris.Context) {
		id, _ := ctx.Params().GetInt("id")
		fid, _ := ctx.Params().GetInt("fid")
		ctx.Writef("hello id : %d looking for friend id : ", id, fid)
	}) // id错会抛出503, fid错会抛出504, 这些都可以自己定义的, 宏出错可以在后面+ "else + errCode", 如果宏没通过则抛出你定义的errCode

	// http://localhost:8080/game/a-zA-Z/level/0-9
	// 记住 : alphabetical 指仅仅允许大小写字母 -> a-zA-Z
	app.Get("/game/{name:alphabetical else 405}/level/{level:int else 403}", func(ctx iris.Context) {
		ctx.Writef("name : %s | level : %s", ctx.Params().Get("name"), ctx.Params().Get("level"))
	})

	// localhost:8080/lowercase/小写name
	// 用正则表达式regexp约束url参数
	app.Get("/lowercase/{name:string regexp(^[a-z]+)}", func(ctx iris.Context) {
		ctx.Writef("name应该小写, 反则这个路径将会404", ctx.Params().Get("name"))
	})

	// http://localhost:8080/single_file/app.js
	app.Get("/single_file/{myfile:file}", func(ctx iris.Context) {
		get := ctx.Params().Get("myfile")
		ctx.JSON(iris.Map{
			"status":  http.StatusOK,
			"data":    get,
			"message": "ok",
		})
	})

	// http://localhost:8080/myfiles/any/directory/here/
	// 这是唯一一个接受热河数量路径片段的宏类型
	app.Get("/myfiles/{directory:path}", func(ctx iris.Context) {
		get := ctx.Params().Get("directory")
		ctx.JSON(iris.Map{
			"status":  http.StatusOK,
			"data":    get,
			"message": "ok",
		})
	})
	app.Run(iris.Addr(":8080"))
}
