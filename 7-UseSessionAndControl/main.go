package main

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/context"
	"github.com/kataras/iris/v12/sessions"
	"github.com/kataras/iris/v12/sessions/sessiondb/boltdb"
)

/*
西方音乐ppt url
https://www.docin.com/p-2422601477.html
*/

var (
	USERNAME string
	ISLOGIN  string
)

func main() {
	app := iris.New()

	sessionID := "mySession" // session由服务器端保管, Cookie由客户端保存

	// 1. 创建session并进行使用
	sess := sessions.New(sessions.Config{
		Cookie: sessionID,
	})

	/**
	用户登录功能
	*/
	app.Post("/login", func(context context.Context) {

		path := context.Path()
		app.Logger().Info("请求path : " + path)
		username := context.PostValue("username")
		password := context.PostValue("password")
		app.Logger().Info(username, password)

		if username == "Morgan" && password == "1355818157" {
			session := sess.Start(context)
			//用户名
			session.Set(USERNAME, username)

			session.Set(ISLOGIN, true)
			context.WriteString("登陆成功")
		} else {
			session := sess.Start(context)
			session.Set(ISLOGIN, false)
			context.WriteString("登录失败")
		}
	})

	/**
	用户退出登录功能
	*/
	app.Get("/logout", func(c context.Context) {
		path := c.Path()
		app.Logger().Info("请求路径为 : ", path)

		session := sess.Start(c)
		//把原本存在的信息删除掉
		session.Delete(ISLOGIN)
		session.Delete(USERNAME)

		c.WriteString("退出登录成功")
	})

	/**
	查询方法
	*/
	app.Get("/queryLogin", func(c context.Context) {
		path := c.Path()
		app.Logger().Info("查询信息 path : " + path)

		session := sess.Start(c)

		isLogin, err := session.GetBoolean(ISLOGIN)
		if err != nil {
			c.WriteString("账户未登录, 请先登录")
			return
		}
		if isLogin {
			app.Logger().Info("账户已登录")
			c.WriteString("账户已登录")
		} else {
			app.Logger().Info("账户未登录")
			c.WriteString("账户未登录")
		}
	})

	// 2. session和db绑定使用
	db, err := boltdb.New("sessions.db", 0600)
	if err != nil {
		panic(err.Error())
	}
	// 程序中断时, 将数据库关闭
	iris.RegisterOnInterrupt(func() {
		defer db.Close()
	})

	//session 和 db绑定
	sess.UseDatabase(db)

	app.Run(iris.Addr(":8080"))
}
