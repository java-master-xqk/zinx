package main

import (
	"encoding/json"
	"fmt"
	"github.com/kataras/iris/v12"
	"os"
)

func main() {
	app := iris.New()

	// 开始配置
	app.Configure(iris.WithConfiguration(iris.Configuration{
		// 如果设置为true, 当人为中断程序执行时, 则不会正常将服务器关闭 如果设置为true则需要钉子已处理
		DisableInterruptHandler: false,

		// 默认为false
		DisablePathCorrection: false,

		EnablePathEscape: false,

		FireMethodNotAllowed: false,

		DisableBodyConsumptionOnUnmarshal: false,

		DisableAutoFireStatusCode: false,

		//  上面一般都是默认的, 下面两个自定义就行
		TimeFormat: "Mon,02 Jan 2006 15:04:05 GMT",
		Charset:    "utf-8",
	}))

	// 通过ini文件进行应用配置,
	// 详细可参考我上一个项目 : https://gitee.com/java-master-xqk/ginblog/blob/master/config/config.ini

	// 通过json配置文件进行应用配置
	var conf ConfigInfo
	jsonPath := "D:\\GoPeoject\\Iris-admin\\5-RouterGroup\\configs\\config.json"
	ParseJsonFile(&conf, jsonPath)

	app.Run(iris.Addr(":8000"))
}

type ConfigInfo struct {
	ProjectName string
	Port        string
}

// ParseJsonFile 用来解析json文件的, 需要传入json文件对应的struct类型指针用来接收信息
// ParseJsonFile 还需要一个json文件的地址 jsonPath
func ParseJsonFile(conf *ConfigInfo, jsonPath string) {
	fileObject, _ := os.Open(jsonPath)
	defer fileObject.Close()
	decoder := json.NewDecoder(fileObject)
	err := decoder.Decode(&conf)
	if err != nil {
		fmt.Println("解析文件失败")
	} else {
		fmt.Println("解析成功")

		//fmt.Println(conf)
		//fmt.Println(conf.Port)
		//fmt.Println(conf.ProjectName)
	}

}