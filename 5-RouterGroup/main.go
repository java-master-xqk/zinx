package main

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/context"
)

/**
路由组 router group
*/

func main() {
	app := iris.New()

	// 用户模块
	// xxx/users/register 注册
	// xxx/users/login 登录
	// xxx/users/info 获取用户信息
	// ...

	/**
	路由组i请求, 相当于创建了一个路由组,
	类似gin里面的 r := gin.Default(), r.Group()
	*/
	userParty := app.Party("/users", func(context context.Context) {
		// 处理下一级请求
		context.Next()
	})

	/**
	路由组下面的下一级请求
	../users/register
	*/
	// 这里的请求路径为 : http://{hostname}:{port}/users/register
	// 其中/users 是 userParty这个路由组下面的全部接口都带有的
	// 而/register则是我们这个接口的
	userParty.Get("/register", func(c context.Context) {
		app.Logger().Info("用户注册功能, 请求路径为 : ", c.Path())
		c.WriteString(c.Path())
	})

	/**
	路由组下面的下一级请求
	../users/login
	*/
	userParty.Get("/login", func(c context.Context) {
		app.Logger().Info("用户登录功能")
		c.WriteString("用户登录功能")
	})

	usersRouter := app.Party("/admin", userMiddleware)

	// Done
	// 路由组内, 只要请求了 context.Next()方法, 那么Done方法就会执行
	usersRouter.Done(func(c context.Context) {
		c.Application().Logger().Infof("response sent to " + c.Path())
	})

	usersRouter.Get("/info", func(c context.Context) {
		c.WriteString("用户信息")
		c.Next() // 手动显式调用
	})

	usersRouter.Get("/query", func(c context.Context) {
		c.WriteString("admin查询")
	})

	app.Run(iris.Addr(":8080"))
}

// todo
func userMiddleware(c context.Context) {
	// 这里是用户路由中间件
	c.Next()
}
