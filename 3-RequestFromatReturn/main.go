package main

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/context"
)

/**
请求数据格式返回
*/

type Teacher struct {
	Name     string `json:"name" xml:"name`
	Password string `json:"password" xml:"password"`
}

func main() {
	app := iris.New()

	/**
	通过GET请求
	返回WriteString
	*/
	app.Get("/getHello", func(context context.Context) {
		context.WriteString("Hello world")
	})

	/**
	通过Get请求
	返回 HTML
	*/
	app.Get("/getHtml", func(c context.Context) {
		c.HTML("<h1>Morgan<h1>")
	})

	/**
	通过Get请求 返回Json
	*/
	app.Get("/getJson", func(c context.Context) {
		c.JSON(iris.Map{
			"status":  200,
			"data":    nil,
			"message": "成功返回Json",
		})
	})

	// Post
	app.Post("/user/info", func(c context.Context) {
		var tec Teacher

		err := c.ReadJSON(&tec)
		if err != nil {
			c.JSON(iris.Map{
				"status":  404,
				"message": "请求异常",
			})
		} else {
			app.Logger().Info(tec)
			c.JSON(iris.Map{
				"status":  200,
				"data":    tec,
				"message": "成功",
			})
			//c.WriteString(tec.Name)
		}
	})

	// Put
	app.Put("getHello", func(c context.Context) {
		c.WriteString("Put Request")
	})
	// .. 不一一列举
	// ...
	// ...
	app.Run(iris.Addr(":8000"))
}
